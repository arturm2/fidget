

import json
import logging
import logging.config

from fidget import Fidget, WebClient, Storage, ImgNamer


def assemble(app_config):
    # todo: change to a proper DI container
    webclient = WebClient(app_config['urlRegexp'])
    storage = Storage(app_config['dbFilepath'])
    img_namer = ImgNamer()
    return Fidget(app_config, webclient, storage, img_namer)


def get_config():
    with open('config.json', 'r') as f:
        return json.load(f)


def setup_logging(logging_config):
    logging.basicConfig()
    logging.config.dictConfig(logging_config)


config = get_config()
setup_logging(config['logging'])

fidget = assemble(config['app'])
fidget.run()
