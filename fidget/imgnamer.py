"""Service to rename jpegs according to their timestamps"""

import os.path
import datetime
import time
import re

import piexif
import fidget.logger

LOGGER = fidget.logger.get_module_logger(__name__)


class ImgNamer:
    """
    Service to rename jpeg files to have timestamp in their names and
    adjust file's modification time
    """

    reDatetime = re.compile(r'\d{8}_\d{6}')
    """Regexp to guess timestamp from file's name"""

    def set_name(self, fpath, index):
        """
        Renames the file to have timestamp in its name and adjusts file's modification time

        Parameters:
        :fpath (str): full path to the file
        :index (int): number of file processed (necessary to avoid name conflicts)

        Returns:
        tuple (string, datetime) where
        - new_name new name of the file
        - found timestamp as datetime

        """
        datetime_tuple, datetime_float = self._extract_timestamp(fpath)
        os.utime(fpath, (datetime_float, datetime_float))   # reset modification time

        path = os.path.split(fpath)[0]
        fname2 = f'{time.strftime("%Y%m%d_%H%M%S", datetime_tuple)}_{index:03}.jpg'
        fpath2 = os.path.join(path, fname2)
        os.rename(fpath, fpath2)
        LOGGER.info('%s renamed as %s', fpath, fname2)

        return fname2, datetime.datetime.fromtimestamp(time.mktime(datetime_tuple))

    @staticmethod
    def _extract_timestamp(fpath):
        """
        Tries to find timestamp of when the photo has been taken.
        Returns tuple (t1, t2) where
        - t1 timestamp tuple
        - t2 timestamp float
        :fpath full path to the file
        """
        exif = piexif.load(fpath)
        datetime_tuple = None  # datetime tuple
        if 'Exif' in exif and piexif.ExifIFD.DateTimeOriginal in exif['Exif']:
            datetime_str = exif['Exif'][piexif.ExifIFD.DateTimeOriginal].decode('ascii')
            datetime_tuple = time.strptime(datetime_str, '%Y:%m:%d %H:%M:%S')
        elif '0th' in exif and piexif.ImageIFD.DateTime in exif['0th']:
            datetime_str = exif['0th'][piexif.ImageIFD.DateTime].decode('ascii')
            datetime_tuple = time.strptime(datetime_str, '%Y:%m:%d %H:%M:%S')
        else:
            match = ImgNamer.reDatetime.search(fpath)
            if match:
                datetime_str = match.group()
                datetime_tuple = time.strptime(datetime_str, '%Y%m%d_%H%M%S')

        return datetime_tuple, time.mktime(datetime_tuple)
