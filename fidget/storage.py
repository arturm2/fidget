"""Persistent storage service"""

import sqlite3
import datetime
import time

import fidget.logger

LOGGER = fidget.logger.get_module_logger(__name__)


class Storage:
    """Persistence service: sqlite database"""

    CREATE_SQL = """
        CREATE TABLE IF NOT EXISTS jpeg (
            url VARCHAR (255) NOT NULL, filename
            VARCHAR (64)  NOT NULL,
            taken_at DATETIME NOT NULL,
            downloaded_at DATETIME NOT NULL);"""

    def __init__(self, filepath):
        """Initialises storage service: creates or opens 'filepath' sqlite database

        Parameters
        :filepath (string): full path of the db file
        """
        self.db_conn = sqlite3.connect(filepath)
        self.db_cur = self.db_conn.cursor()
        self.db_cur.execute(Storage.CREATE_SQL)
        self.db_conn.commit()

    EXISTS_SQL = 'select count(*) from jpeg where url = ?'

    def exists(self, url):
        """Checks if the url has already been downloaded"""
        self.db_cur.execute(Storage.EXISTS_SQL, (url,))
        row = self.db_cur.fetchone()
        count = int(row[0])
        return count > 0

    SAVE_SQL = 'insert into jpeg (url, filename, taken_at, downloaded_at) values (?, ?, ?, ?)'

    def save(self, url, fname2, timestamp):
        """Saves the url as already downloaded, additionally saves new file's name and mod time"""
        tup = (url, fname2, timestamp, datetime.datetime.fromtimestamp(time.time()))
        self.db_cur.execute(Storage.SAVE_SQL, tup)
        self.db_conn.commit()
        LOGGER.debug('%s saved', url)
