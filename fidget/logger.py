"""Logging helper"""

import logging


def get_module_logger(module_name):
    """Convenience function to get a module's logger"""
    return logging.getLogger(module_name)
