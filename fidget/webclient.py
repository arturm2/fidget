"""Service for handling http requests and parsing html"""

import shutil
import re
import requests

import fidget.logger

LOGGER = fidget.logger.get_module_logger(__name__)


class WebClient:
    """Web client and html parsing service"""

    host = 'perelkawroclaw.pl'
    loginPath = 'wp-login.php?action=postpass'
    pagePath = '501-2'
    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Encoding': 'gzip, deflate',
        'Accept-Language': 'pl,en;q=0.8',
        'Cache-Control': 'no-cache',
        'Connection': 'keep-alive',
        'Pragma': 'no-cache',
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 ' +
                      '(KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
        'Upgrade-Insecure-Requests': '1'}
    loginData = {
        'post_password': '',
        'Submit': 'Zatwierdź' }

    def __init__(self, url_regexp):
        self.url_regexp = re.compile(url_regexp)
        self.session = requests.Session()

    def login(self, password):
        """Logs in to the page"""
        WebClient.loginData['post_password'] = password
        LOGGER.info('Logging in')
        self.session.post(self._get_url(self.loginPath), data=self.loginData,
                          headers=self._get_headers())

    def get_index_page(self):
        """Retrieves the html page with the list of urls to grab"""
        LOGGER.info('grabbing urls')
        url = self._get_url(self.pagePath)
        resp = self.session.get(url)

        if resp.status_code != 200:
            raise RuntimeError(f'Could not grab {url}, status code = {resp.status_code}')

        content = resp.content.decode('utf-8')
        return content

    def download(self, url, fpath):
        """Downloads the image at url and saves as fpath"""
        LOGGER.info('Downloading %s', url)

        # the site is tricky, for non-existing urls it yields 302, we ban redirects then
        resp = self.session.get(url, headers=self._get_headers(), stream=True,
                                allow_redirects=False)

        if resp.status_code != 200:
            raise RuntimeError(f'Could not grab {url}, status code = {resp.status_code}')

        file = open(fpath, 'wb')
        resp.raw.decode_content = True
        shutil.copyfileobj(resp.raw, file)
        file.close()
        size = ''
        if 'content-length' in resp.headers.keys():
            size = f' ({resp.headers["content-length"]} bytes)'
        LOGGER.info('Saved as %s%s', fpath, size)

    def _get_url(self, path=None):
        """Renders url string"""
        return f'https://{self.host}/{path}' if path else f'http://{self.host}'

    def _get_headers(self):
        """Gets headers for the http request"""
        if not self.headers:
            self.headers = WebClient.headers
            self.headers['Host'] = self.host
            self.headers['Origin'] = self._get_url()
            self.headers['Referer'] = self._get_url(self.pagePath)
        return self.headers

    def find_urls(self, page):
        """Finds all urls to download in given page"""
        urls = self.url_regexp.findall(page)
        LOGGER.debug('Found %d urls', len(urls))
        return urls
