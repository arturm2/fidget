"""Fancybox Gallery Image Getter main class"""
import os.path

import fidget.logger

LOGGER = fidget.logger.get_module_logger(__name__)


class Fidget:
    """Fancybox Gallery Image Downloader"""

    def __init__(self, config, webclient, storage, img_namer):
        self.total = None
        self.config = config
        self.webclient = webclient
        self.storage = storage
        self.img_namer = img_namer

    def run(self):
        """Main method"""
        self.webclient.login(self.config['clientPassword'])
        page = self.webclient.get_index_page()
        urls = self.webclient.find_urls(page)

        i = 0
        self.total = len(urls)
        for url in urls:
            # noinspection PyBroadException
            try:
                self._process_url(url, i)
            except Exception:   # high level, general catch, we don't need anything fancier
                LOGGER.error('Error while handling %s', url, exc_info=True)
            i = i + 1

    def _process_url(self, url, index):
        """Processes single url"""
        fname = url[url.rfind('/') + 1:]    # original file name
        LOGGER.info('Processing %s (%s of %s)', fname, index + 1, self.total)

        # check if exists
        exists = self.storage.exists(url)
        if exists:
            LOGGER.debug('%s already exists', fname)
            return

        # download
        fpath = os.path.join(self.config['downloadDir'], fname)     # full path to target file
        self.webclient.download(url, fpath)

        # change name to timestamp
        fname2, timestamp = self.img_namer.set_name(fpath, index)

        # save
        self.storage.save(url, fname2, timestamp)
