
import os
import unittest
from fidget import WebClient


THIS_DIR = os.path.dirname(os.path.abspath(__file__))


class TestWebClient(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestWebClient, self).__init__(*args, **kwargs)
        self.client = WebClient("<a href=\"([^\"]+)\" class=\"pb-image\"")

    def test_find_urls(self):
        path = os.path.join(THIS_DIR, 'http _perelkawroclaw.pl_501-2_.html')
        with open(path, 'r', encoding='utf-8') as f:
            content = f.read()

        urls = self.client.find_urls(content)
        self.assertEqual(len(urls), 189)

    # todo: add more unit tests


if __name__ == '__main__':
    unittest.main()
