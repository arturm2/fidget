
import os
import unittest
from unittest.mock import Mock, MagicMock

from fidget import Fidget, WebClient


THIS_DIR = os.path.dirname(os.path.abspath(__file__))

# todo: test it all better :)
class TestFidget(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestFidget, self).__init__(*args, **kwargs)

        # WebClient mock
        path = os.path.join(THIS_DIR, 'http _perelkawroclaw.pl_501-2_.html')
        with open(path, 'r', encoding='utf-8') as f:
            content = f.read()
        self.webclient = WebClient("<a href=\"([^\"]+)\" class=\"pb-image\"")
        self.webclient.get_index_page = Mock(return_value=content)
        self.webclient.download = Mock()
        # Storage mock
        self.storage = Mock()
        self.storage.exists.return_value = False
        # Config mock
        config = { 'downloadDir': '', 'clientPassword': '' }
        # ImgNamer
        self.img_namer = MagicMock()
        self.img_namer.set_name = Mock(return_value=('', ''))

        self.fidget = Fidget(config, self.webclient, self.storage, self.img_namer)

    def test_find_urls(self):
        self.fidget.run()
        self.assertEqual(self.storage.save.call_count, 189)
        self.assertEqual(self.webclient.download.call_count, 189)
        self.assertEqual(self.img_namer.set_name.call_count, 189)


if __name__ == '__main__':
    unittest.main()
